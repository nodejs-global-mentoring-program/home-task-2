import express from 'express';
import Joi, { required } from 'joi';
import { v4 as uuidv4 } from 'uuid';

const app = express();
app.use(express.urlencoded({ extended: false }));
app.use(express.json())
const port = 3000;

const users = [];

const userSchema = Joi.object({
    id: Joi.string()
        .pattern(new RegExp('[0-9A-Za-z]{8}-[0-9A-Za-z]{4}-4[0-9A-Za-z]{3}-[89ABab][0-9A-Za-z]{3}-[0-9A-Za-z]{12}'))
        .required(),

    login: Joi.string()
        .required(),

    password: Joi.string()
        .alphanum()
        .required(),

    age: Joi.number()
        .integer()
        .min(4)
        .max(130)
        .required(),

    isDeleted: Joi.boolean()
        .required()
});

const getAutoSuggestUsers = (loginSubstring, limit) => {
    console.log(loginSubstring, limit)
    const filteredUsers = users.filter(user => user.login.indexOf(loginSubstring) >= 0 && user.isDeleted === false).sort((a, b) => {
        const loginA = a.login.toUpperCase();
        const loginB = b.login.toUpperCase();
        return (loginA < loginB) ? -1 : (loginA > loginB) ? 1 : 0;
    });
    return limit ? filteredUsers.slice(0, parseInt(limit)) : filteredUsers;
}
app.use((req, res, next) => {
    console.log(req.method, req.url);
    next();
});

app.get('/users/auto-suggest', (req, res) => {
    const filteredUsers = getAutoSuggestUsers(req.query.loginSubstring, req.query.limit) ;
    return res.status(200).send(filteredUsers);
});

app.get('/users/:id', (req, res) => {
    const user = users.find(user => user.id === req.params.id && user.isDeleted === false);
    if (user) {
        return res.status(200).json(user);
    }
    return res.status(404).send('User not found')
});

app.post('/users/', async (req, res) => {
    try {
        await userSchema.validateAsync(req.body);
        const user = req.body;
        user.isDeleted = false;
        users.push(user);
        return res.status(201).send('User created');
    }
    catch (err) {
        return res.status(400).json(err.details);
    }
});

app.put('/users/:id', async (req, res) => {
    try {
        await userSchema.validateAsync(req.body);
        const indexUser = users.findIndex(user => user.id === req.params.id && user.isDeleted === false);
        if (indexUser >= 0) {
            const userUpdate = req.body;
            userUpdate.isDeleted = false;
            users[indexUser] = userUpdate;
            return res.status(200).send('User updated');
        }
        return res.status(404).send('User not found');
    }
    catch (err) {
        return res.status(400).json(err.details);
    }
});

app.delete('/users/:id', async (req, res) => {
    const indexUser = users.findIndex(user => user.id === req.params.id);
    if (indexUser >= 0) {
        users[indexUser].isDeleted = true;
        return res.status(200).send('User deleetd');
    }
    return res.status(404).send('User not found');
});

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`);
})